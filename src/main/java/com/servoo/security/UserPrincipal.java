package com.servoo.security;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.servoo.model.Role;
import com.servoo.model.User;

import lombok.Data;

@Data
public class UserPrincipal implements UserDetails {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private String name;
	private String email;
	private String phone;
	private String phone1;
	private String phone2;
	private String fax;
	private String password;
	private String picture;
	private boolean active;
	private boolean deleted;
    private Date createdAt;
    private Date modifiedAt;
	private Collection<? extends GrantedAuthority> authorities;
    private Set<Role> roles;
	
	public UserPrincipal() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserPrincipal(Long id, String name, String email, String phone, String phone1, String phone2, String fax, String password,
			 String picture, boolean active, boolean deleted, Date createdAt, Date modifiedAt,
			Collection<? extends GrantedAuthority> authorities) {
		super();
		this.id = id;

		this.name = name;
		this.email = email;
		this.phone = phone;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.fax = fax;
		this.password = password;
		this.picture = picture;
		this.active = active;
		this.deleted = deleted;
		this.authorities = authorities;
	}

	public static UserPrincipal create(User user) {
        List<GrantedAuthority> authorities = user.getRoles().stream().map(role ->
        	new SimpleGrantedAuthority(role.getName())
        ).collect(Collectors.toList());
        
        return new UserPrincipal(
                user.getId(),
                user.getName(),
                user.getEmail(),
                user.getPhone(),
                user.getPhone1(),
                user.getPhone2(),
                user.getFax(),
                user.getPassword(),
                user.getPicture(),
                user.isActive(),
                user.isDeleted(),
                user.getCreatedAt(),
                user.getModifiedAt(),
                authorities
        );
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return this.authorities;
	}
	

	@Override
	public String getPassword() {
		return this.password;
	}

	@Override
	public String getUsername() {
		return this.email != "" ? this.email : this.phone;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return this.isActive();
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
	


}
