package com.servoo.security;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.servoo.model.User;
import com.servoo.repository.UserRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String usernameOrEmail) throws UsernameNotFoundException {
		User user = this.userRepository.findByPhoneOrEmail(usernameOrEmail, usernameOrEmail)
				.orElseThrow(()-> 
							new UsernameNotFoundException("L'utilisateur avec l'email ou le téléphone :"
									+usernameOrEmail+ "n'existe pas"));
		
		return UserPrincipal.create(user);
	}
	
	// This method is used by JWTAuthenticationFilter
    @Transactional
    public UserDetails loadUserById(Long id) {
        User user = userRepository.findById(id)
        		.orElseThrow(() -> new UsernameNotFoundException("L'utilisateur avec l'id " + id +" n'a pas été trouvé"));

        return UserPrincipal.create(user);
    }

	
}
