package com.servoo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.servoo.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	Optional<User> findByEmail(String email);

	Optional<User> findByPhoneOrEmail(String phone, String email);
	
	Optional<User> findByPhone(String phone);
	
	Optional<User> findByPhoneAndActive(String phone, boolean active);
	
	Optional<User> findByPhoneOrEmailAndActive(String phone, String email, boolean active);
	
	@Query("SELECT u FROM User u ORDER BY id DESC")
	List<User> findAll();
	
	@Query("SELECT u FROM User u ORDER BY id DESC")
	Page<User> findAll(Pageable pageable);
	
	Boolean existsByEmail(String email);
	
	Boolean existsByPhone(String phone);
	
	Boolean existsByIdAndActive(Long id, boolean active);
	
	Boolean existsByPhoneOrEmailAndActive(String phone, String email, boolean active); 
	
	Boolean existsByPhoneAndActive(String phone, boolean active); 
	
	Boolean existsByPhoneOrEmail(String phone, String email); 
	
	
}
