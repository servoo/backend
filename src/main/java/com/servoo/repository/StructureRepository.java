package com.servoo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.servoo.model.Structure;

@Repository
public interface StructureRepository extends CrudRepository<Structure, Long> {

	@Query("SELECT s FROM Structure s ORDER BY id DESC")
	List<Structure> findAll();
}
