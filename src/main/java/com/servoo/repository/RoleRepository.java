package com.servoo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.servoo.model.Role;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long>{

	Optional<Role> findByName(String nom);
	
	Boolean existsByName(String nom);
	
	Boolean existsByIdAndActive(Long id, int actif);
	
	Boolean existsByNameAndActive(String nom, int actif);
	
	Boolean existsByIdAndName(Long id, String nom);
	
	@Query("SELECT r FROM Role r WHERE r.active=1 ORDER BY id DESC")
	List<Role> findAllByActive();
}
