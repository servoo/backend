package com.servoo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.servoo.model.Structure;
import com.servoo.model.Vehicle;

@Repository
public interface VehicleRepository extends CrudRepository<Vehicle, Long> {

	@Query("SELECT v FROM Vehicle v ORDER BY id DESC")
	List<Vehicle> findAll();
	
	List<Vehicle> findByStructure(Structure structure);
	
}
