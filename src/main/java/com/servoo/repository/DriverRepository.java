package com.servoo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.servoo.model.Driver;

@Repository
public interface DriverRepository extends CrudRepository<Driver, Long> {

	@Query("SELECT d FROM Driver d ORDER BY id DESC")
	List<Driver> findAll();
	
}
