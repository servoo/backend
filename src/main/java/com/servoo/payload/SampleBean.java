package com.servoo.payload;

import lombok.Data;

@Data
public class SampleBean {
	String name;
	String color;
}
