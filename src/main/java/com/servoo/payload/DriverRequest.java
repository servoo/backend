package com.servoo.payload;

import java.util.Date;

import org.springframework.lang.Nullable;

import lombok.Data;

@Data
public class DriverRequest {

	private Long id;
	
	private String name;
	
	private String phone;
	
	private String email;
	
	private String picture;
	
	private String address;
	
	private String licenceRef;
	
	private boolean active;
	
	private boolean deleted;
	
	private Date licenceDateOfIssuance;
	
	private Date licenceDateOfExpiry;

	@Nullable
	private String licenceDateOfIssuanceText;
	
	@Nullable
	private String licenceDateOfExpiryText;
	
	
}
