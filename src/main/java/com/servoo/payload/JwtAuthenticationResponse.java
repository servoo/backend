package com.servoo.payload;

import lombok.Data;

@Data
public class JwtAuthenticationResponse {

	private String accessToken;
	private SignUpRequest data;
    private String tokenType = "Bearer";

	public JwtAuthenticationResponse(String accessToken) {
		super();
		this.accessToken = accessToken;
	}
    
	public JwtAuthenticationResponse(String accessToken, SignUpRequest user) {
		super();
		this.data = user;
		this.accessToken = accessToken;
	}
	
	public JwtAuthenticationResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
    
    
}
