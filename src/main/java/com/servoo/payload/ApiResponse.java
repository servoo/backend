package com.servoo.payload;

import lombok.Data;

@Data
public class ApiResponse {

	private Boolean success;
	private String message;
	private Object data;
	
	public ApiResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ApiResponse(Boolean success, String message) {
		super();
		this.success = success;
		this.message = message;
	}
	
	public ApiResponse(Boolean success, String message, Object data) {
		super();
		this.success = success;
		this.message = message;
		this.data = data;
	}

	
	
	
}
