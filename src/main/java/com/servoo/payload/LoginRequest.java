package com.servoo.payload;

import lombok.Data;

@Data
public class LoginRequest {
	
	private String password;
	
	private String phoneOrEmail;

}
