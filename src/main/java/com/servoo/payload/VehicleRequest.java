package com.servoo.payload;

import java.util.Date;

import com.servoo.model.Driver;
import com.servoo.model.Structure;
import com.servoo.model.Vehicle;

import lombok.Data;

@Data
public class VehicleRequest {
	
	private Long id;
	
	private String registrationNumber;
	
	private double capacityWeight;
	
	private double emptyWeight;
	
	private double totalWeight;
	
	private Date firstCircul;

	private String firstCirculText;
	
	private String ownGPSDevice;
	
	private boolean isAvailable;
	
	private String description;
	
	private boolean active;
	
	private boolean deleted;
	
	private Structure structure;
	
	private Driver driver;
	
	public VehicleRequest() { }
	
	public VehicleRequest(Vehicle v) { 
		this.id = v.getId();
		this.registrationNumber = v.getRegistrationNumber();
		this.capacityWeight = v.getCapacityWeight();
		this.totalWeight = v.getTotalWeight();
		this.ownGPSDevice = v.getOwnGPSDevice();
		this.isAvailable = v.isAvailable();
		this.active = v.isActive();
		this.deleted = v.isDeleted();
		this.structure = v.getStructure();
		this.driver = v.getDriver();
	}
	
	
}
