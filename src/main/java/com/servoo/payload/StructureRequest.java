package com.servoo.payload;

import java.util.Date;
import java.util.List;

import com.servoo.model.User;
import com.servoo.model.Vehicle;

import lombok.Data;

@Data
public class StructureRequest {


	private Long id;
	
	private String name;
	
	private String address1;
	
	private String email;
	
	private String address2;
	
	private String address;
	
	private String phone;
	
	private String fax;
	
	private String website;
	
	private String otherInformations;
	
	private String shortName;
	
	private String isMainLFBureau;
	
	private String description;
	
	private String niu;
	
	private boolean active;
	
	private boolean deleted;
	
	private boolean postPaid;
	
	private boolean individualsOnly;
	
	private Date licenceDateOfIssuance;
	
	private Date licenceDateOfExpiry;

	private String licenceDateOfIssuanceText;
	
	private String licenceDateOfExpiryText;
	
	List<User> users;
	
	List<Vehicle> vehicles;
	
}
