package com.servoo.payload;

import java.util.Date;
import java.util.Set;

import com.servoo.model.Role;
import com.servoo.model.Structure;
import com.servoo.model.User;

import lombok.Data;

@Data
public class SignUpRequest {
	
	private Long id;
	
	private String name;

	private String email;

	private String phone;
	
	private String phone1;
	
	private String phone2;
	
	private String fax;

	private String password;
	
	private String tin;
	
	private String picture;
	
	//Permit us/them to lock or unlock user's account
	private boolean active;
	
	private boolean deleted;

	private Long idStructure;
	
    private Date createdAt;
    
    private Date modifiedAt;
    
    private String roleName;
    
    private Structure structure;
    
    private Set<Role> roles;
    
    public SignUpRequest() {}
    
    public SignUpRequest(User u) {
    	this.id = u.getId();
    	this.name = u.getName();
    	this.deleted = u.isDeleted();
    	this.active = u.isActive();
    	this.createdAt = u.getCreatedAt();
    	this.modifiedAt = u.getModifiedAt();
    	this.roles = u.getRoles();
    	this.phone = u.getPhone();
    	this.phone1 = u.getPhone1();
    	this.phone2 = u.getPhone2();
    	this.fax = u.getFax();
    	this.structure = u.getStructure();
    }
    


}
