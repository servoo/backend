package com.servoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServooApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServooApplication.class, args);
	}

}
