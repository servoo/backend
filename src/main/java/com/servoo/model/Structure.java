package com.servoo.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.servoo.payload.StructureRequest;
import com.sun.istack.NotNull;

import lombok.Data;

@Table(name = "structures")
@Entity
@Data
public class Structure {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String name;

	@NotNull
	private String address1;
	
	private String email;
	
	private String address2;
	
	private String address;
	
	private String phone;
	
	private String fax;
	
	private String website;
	
	private String otherInformations;
	
	private String shortName;
	
	private String isMainLFBureau;
	
	private String description;
	
	@NotNull
	private String niu;
	
	private boolean active;
	
	private boolean deleted;
	
	private boolean postPaid;
	
	private boolean individualsOnly;
	
	//*******RELATIONS*******//
	
	@OneToMany(mappedBy = "structure")
	List<User> users;
	
	@OneToMany(mappedBy = "structure")
	List<Vehicle> vehicles;

	//*******CONSTRUCTORS*******//

	public Structure(String name, String address1, String email, String address2, String address, String phone,
			String fax, String website, String otherInformations, String shortName, String isMainLFBureau,
			String description, String niu, boolean active, boolean deleted, boolean postPaid,
			boolean individualsOnly) {
		super();
		this.name = name;
		this.address1 = address1;
		this.email = email;
		this.address2 = address2;
		this.address = address;
		this.phone = phone;
		this.fax = fax;
		this.website = website;
		this.otherInformations = otherInformations;
		this.shortName = shortName;
		this.isMainLFBureau = isMainLFBureau;
		this.description = description;
		this.niu = niu;
		this.active = false;
		this.deleted = false;
		this.postPaid = postPaid;
		this.individualsOnly = individualsOnly;
	}


	public Structure() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public void setAll(StructureRequest sr) {
		this.name = sr.getName();
		this.address1 = sr.getAddress1();
		this.email = sr.getEmail();
		this.address2 = sr.getAddress2();
		this.address = sr.getAddress();
		this.phone = sr.getPhone();
		this.fax = sr.getFax();
		this.website = sr.getWebsite();
		this.otherInformations = sr.getOtherInformations();
		this.shortName = sr.getShortName();
		this.isMainLFBureau = sr.getIsMainLFBureau();
		this.description = sr.getDescription();
		this.niu = sr.getNiu();
		this.active = false;
		this.deleted = false;
		this.postPaid = sr.isPostPaid();
		this.individualsOnly = sr.isIndividualsOnly();
	}
	
	

}
