package com.servoo.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.servoo.payload.SignUpRequest;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;

import lombok.Data;

@Table(
		name="users",
		uniqueConstraints = @UniqueConstraint(columnNames = {"phone", "email"})
)
@Entity
@Data
public class User {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String name;

	private String email;

	@NotNull
	private String phone;
	
	private String phone1;
	
	private String phone2;
	
	private String fax;

	private String password;
	
	private String picture;
	
	//Permit us/them to lock or unlock user's account
	private boolean active;
	
	private boolean deleted;
	
	@CreationTimestamp
	@Temporal(TemporalType.DATE)
	private Date createdAt;
	
	@UpdateTimestamp
	@Temporal(TemporalType.DATE)
	private Date modifiedAt;
	
	
	//*****RELATIONS*******//

	@ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	@JoinTable(name="user_roles", 
			joinColumns = @JoinColumn(name="user_id"),
			inverseJoinColumns=@JoinColumn(name="role_id"))
	@LazyCollection(LazyCollectionOption.FALSE)
	private Set<Role> roles= new HashSet<>();
	
	@ManyToOne
	@JoinColumn(name = "structure.id")
	@JsonIgnore
	@Nullable
	private Structure structure;
	
	//*******CONSTRUCTORS********//
	

	public User() {
		super();
	}
	
	//*******OTHERS METHODS********//
	
	public void setAll(SignUpRequest s) {
		this.name = s.getName();
		this.email = s.getEmail();
		this.phone = s.getPhone();
		this.phone1 = s.getPhone1();
		this.phone2 = s.getPhone2();
		this.fax = s.getFax();
		this.picture = s.getPicture();
		this.active = true;
		this.deleted = false;
	}
	
	public User(String name, String email, String phone, String phone1, String phone2, String fax, String password,
			String tin, String picture, boolean active, boolean deleted) {
		super();
		this.name = name;
		this.email = email;
		this.phone = phone;
		this.phone1 = phone1;
		this.phone2 = phone2;
		this.fax = fax;
		this.picture = picture;
		this.active = active;
		this.deleted = deleted;
	}

}
