package com.servoo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Entity
@Data
@Table(
		name="roles",
		uniqueConstraints = @UniqueConstraint(columnNames = {"name"})
)
public class Role {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@Column(length=20)
	private String name; 
	
	private int active = 1;
	
	public Role(String name) {
		super();
		this.name = name;
		this.active = 1;
	}

	public Role() {
		super();
		this.active = 1;
	}
}