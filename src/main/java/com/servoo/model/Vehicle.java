package com.servoo.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.servoo.payload.VehicleRequest;
import com.sun.istack.NotNull;

import lombok.Data;

@Table(name = "vehicles")
@Entity
@Data
public class Vehicle {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String registrationNumber;
	
	private double capacityWeight;
	
	private double emptyWeight;
	
	private double totalWeight;
	
	private String ownGPSDevice;
	
	private boolean isAvailable;
	
	private String description;
	
	private boolean active;
	
	private boolean deleted;
	
	@ManyToOne
	@JoinColumn(name = "structure.id")
	@JsonIgnore
	private Structure structure;
	
	@ManyToOne
	@JoinColumn(name = "driver.id")
	@JsonIgnore
	private Driver driver;

	public Vehicle(String registrationNumber, double capacityWeight, double emptyWeight, double totalWeight,
			Date firstCircul, String ownGPSDevice, boolean isAvailable, String description, boolean active,
			boolean deleted) {
		super();
		this.registrationNumber = registrationNumber;
		this.capacityWeight = capacityWeight;
		this.emptyWeight = emptyWeight;
		this.totalWeight = totalWeight;
		this.ownGPSDevice = ownGPSDevice;
		this.isAvailable = false;
		this.description = description;
		this.active = false;
		this.deleted = false;
	}

	public Vehicle() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Vehicle(VehicleRequest vr) {
		super();
		this.registrationNumber = vr.getRegistrationNumber();
		this.capacityWeight = vr.getCapacityWeight();
		this.emptyWeight = vr.getEmptyWeight();
		this.totalWeight = vr.getTotalWeight();
		this.ownGPSDevice = vr.getOwnGPSDevice();
		this.isAvailable = false;
		this.description = vr.getDescription();
		this.active = false;
		this.deleted = false;
	}
	
}
