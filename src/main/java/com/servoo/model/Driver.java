package com.servoo.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.servoo.payload.DriverRequest;
import com.sun.istack.NotNull;

import lombok.Data;

@Table(name = "drivers")
@Entity
@Data
public class Driver {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
	private String name;
	
	private String phone;
	
	private String email;
	
	private String picture;
	
	private String address;
	
	private String licenceRef;
	
	private boolean active;
	
	private boolean deleted;
	
	//*******RELATIONS*******//
	
	@OneToMany(mappedBy = "driver")
	List<Vehicle> vehicles;

	public Driver(String name, String phone, String email, String picture, String address, String licenceRef,
			boolean active, boolean deleted) {
		super();
		this.name = name;
		this.phone = phone;
		this.email = email;
		this.picture = picture;
		this.address = address;
		this.licenceRef = licenceRef;
		this.active = true;
		this.deleted = false;
	}

	public Driver() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Driver(DriverRequest dr) {
		super();
		this.name = dr.getName();
		this.phone = dr.getPhone();
		this.email = dr.getEmail();
		this.picture = dr.getPicture();
		this.address = dr.getAddress();
		this.licenceRef = dr.getLicenceRef();
		this.active = true;
		this.deleted = false;
	}
	
	
	
	
}
