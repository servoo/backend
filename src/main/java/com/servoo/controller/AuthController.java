package com.servoo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.servoo.model.User;
import com.servoo.payload.ApiResponse;
import com.servoo.payload.JwtAuthenticationResponse;
import com.servoo.payload.LoginRequest;
import com.servoo.payload.SignUpRequest;
import com.servoo.repository.UserRepository;
import com.servoo.security.JwtTokenProvider;

@RestController
@RequestMapping("/auth-service")
@CrossOrigin("*")
//@Api(value="auth-service", description="Operations linked to user authentication stuff") if using Swagger doc
public class AuthController {

	@Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    JwtTokenProvider tokenProvider;
    
    @Autowired
    PasswordEncoder passwordEncoder;
	
//    @ApiOperation(value = "Login api", response = ResponseEntity.class)
	@PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@RequestBody LoginRequest loginRequest) {
		
		if(!this.userRepository.existsByPhoneOrEmail(loginRequest.getPhoneOrEmail(), loginRequest.getPhoneOrEmail())) {
    		return new ResponseEntity<Object>(new ApiResponse(false, "Compte inexistant"),
    				HttpStatus.BAD_REQUEST);
    	}
    	
    	if (!this.userRepository.existsByPhoneOrEmail(loginRequest.getPhoneOrEmail(), loginRequest.getPhoneOrEmail())) {
    		return new ResponseEntity<Object>(new ApiResponse(false, "Identifiants incorrects!"),
    				HttpStatus.BAD_REQUEST);
    	}
    	
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getPhoneOrEmail(),
                        loginRequest.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = tokenProvider.generateToken(authentication);
        User user = this.userRepository.findByPhoneOrEmailAndActive(loginRequest.getPhoneOrEmail(), loginRequest.getPhoneOrEmail(), true).get();

    	SignUpRequest s = new SignUpRequest(user);
    	
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, s));
    }
	
}
