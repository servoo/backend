package com.servoo.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.servoo.model.Driver;
import com.servoo.model.Structure;
import com.servoo.model.Vehicle;
import com.servoo.payload.ApiResponse;
import com.servoo.payload.VehicleRequest;
import com.servoo.repository.DriverRepository;
import com.servoo.repository.StructureRepository;
import com.servoo.repository.VehicleRepository;

@RestController
@RequestMapping("/vehicle-service")
@CrossOrigin("*")
public class VehicleController {

	@Autowired
	private VehicleRepository vehicleRepository;

	@Autowired
	private DriverRepository driverRepository;

	@Autowired
	private StructureRepository structureRep;
	
	@PostMapping("/create")
    public ResponseEntity<?> createVehicle(@RequestBody VehicleRequest vr) {
        
        Vehicle v = new Vehicle(vr);
        
        Driver d = this.driverRepository.findById(vr.getDriver().getId()).get();
        Structure s = this.structureRep.findById(vr.getStructure().getId()).get();
        v.setDriver(d);
        v.setStructure(s);
        
        Vehicle vehicleSaved = this.vehicleRepository.save(v);

        return ResponseEntity.ok().body(new ApiResponse(true, "", vehicleSaved));
    }
	

	@GetMapping("")
    public List<VehicleRequest> getVehicle() {
        
        List<Vehicle> vehicles = (List<Vehicle>) this.vehicleRepository.findAll();
        List<VehicleRequest> vehiclesR = new ArrayList<>();
        for (Vehicle v: vehicles) {
        	VehicleRequest vr = new VehicleRequest(v);
        	vehiclesR.add(vr);
        }

        return vehiclesR;
    }

	@GetMapping("/find-by-structure/{id}")
    public List<VehicleRequest> getVehicleByStructure(@PathVariable("id") Long id) {

        Structure s = this.structureRep.findById(id).get();
        List<Vehicle> vehicles = (List<Vehicle>) this.vehicleRepository.findByStructure(s);
        List<VehicleRequest> vehiclesR = new ArrayList<>();
        for (Vehicle v: vehicles) {
        	VehicleRequest vr = new VehicleRequest(v);
        	vehiclesR.add(vr);
        }

        return vehiclesR;
    }

	@PutMapping("/block-or-unblock/{id}")
    public ResponseEntity<?> blockOrUnblock(@PathVariable("id") Long id) throws IOException {
    	Vehicle v = this.vehicleRepository.findById(id).get();
    	if (v.isActive() == true) 
    		v.setActive(false);
    	else
    		v.setActive(true);
    	
    	this.vehicleRepository.save(v);
    	return ResponseEntity.ok().body(new ApiResponse(true, "", v));
    }
	
}
