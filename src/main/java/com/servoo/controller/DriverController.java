package com.servoo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.servoo.model.Driver;
import com.servoo.payload.ApiResponse;
import com.servoo.payload.DriverRequest;
import com.servoo.repository.DriverRepository;

@RestController
@RequestMapping("/driver-service")
@CrossOrigin("*")
public class DriverController {
	
	@Autowired
	private DriverRepository driverRepository;
	
	@PostMapping("/create")
    public ResponseEntity<?> createDriver(@RequestBody DriverRequest dr) {
        if(dr.getName() == "") {
            return new ResponseEntity<Object>(new ApiResponse(false, "Veuillez renseigner le nom du chauffeur"),
                    HttpStatus.BAD_REQUEST);
        }
        
        Driver d = new Driver(dr);
        
        Driver driverSaved = this.driverRepository.save(d);

        return ResponseEntity.ok().body(new ApiResponse(true, "", driverSaved));
    }
	

	@GetMapping("")
    public List<Driver> getAll() {

        List<Driver> drivers = (List<Driver>) this.driverRepository.findAll();

        return drivers;
    }
	

}
