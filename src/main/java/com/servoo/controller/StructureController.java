package com.servoo.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.servoo.model.Structure;
import com.servoo.payload.ApiResponse;
import com.servoo.payload.StructureRequest;
import com.servoo.repository.StructureRepository;

@RestController
@CrossOrigin("*")
@RequestMapping("/structure-service")
public class StructureController {
	
	@Autowired
	private StructureRepository structureRep;
	
	@PostMapping("/create")
    public ResponseEntity<?> createStructure(@RequestBody StructureRequest sr) {
        if(sr.getName() == "" || sr.getAddress() == "" || sr.getNiu() == "") {
            return new ResponseEntity<Object>(new ApiResponse(false, "Veuillez renseigner le nom, l'adresse et le NIU de la structure"),
                    HttpStatus.BAD_REQUEST);
        }
        
        Structure s = new Structure();
        s.setAll(sr);
        
        Structure strucSaved = this.structureRep.save(s);

        return ResponseEntity.ok().body(new ApiResponse(true, "", strucSaved));
    }
	
	@GetMapping("")
    public List<Structure> getAll() {

        List<Structure> structures = (List<Structure>) this.structureRep.findAll();

        return structures;
    }
	
	@PutMapping("/block-or-unblock/{id}")
    public ResponseEntity<?> blockOrUnblock(@PathVariable("id") Long id) throws IOException {
    	Structure s = this.structureRep.findById(id).get();
    	if (s.isActive() == true) 
    		s.setActive(false);
    	else
    		s.setActive(true);
    	
    	this.structureRep.save(s);
    	return ResponseEntity.ok().body(new ApiResponse(true, "", s));
    }

}
