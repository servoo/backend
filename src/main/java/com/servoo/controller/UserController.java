package com.servoo.controller;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.servoo.exception.ResourceNotFoundException;
import com.servoo.model.Role;
import com.servoo.model.Structure;
import com.servoo.model.User;
import com.servoo.payload.ApiResponse;
import com.servoo.payload.SignUpRequest;
import com.servoo.repository.RoleRepository;
import com.servoo.repository.StructureRepository;
import com.servoo.repository.UserRepository;
import com.servoo.security.CurrentUser;
import com.servoo.security.UserPrincipal;

@RestController
@RequestMapping("/auth-service")
@CrossOrigin("*")
public class UserController {

    @Autowired
    private UserRepository userRepository;
    
    @Autowired
    private RoleRepository roleRepository;
    
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private StructureRepository structureRep;


    @GetMapping("/user/me")
    public SignUpRequest getCurrentUser(@CurrentUser UserPrincipal currentUser) {
    	User user = userRepository.findByPhoneOrEmail(currentUser.getPhone() != "" ? currentUser.getPhone() : currentUser.getEmail(), 
    			currentUser.getEmail())
                .orElseThrow(() -> new ResourceNotFoundException("L'utilisateur", "username or email", currentUser.getUsername()));
        
    	SignUpRequest s = new SignUpRequest(user);
    	
        return s;
    }
    
	@PostMapping("/register")
    public ResponseEntity<?> registerUser(@RequestBody SignUpRequest signUpRequest) {
        if(userRepository.existsByPhone(signUpRequest.getPhone())) {
            return new ResponseEntity<Object>(new ApiResponse(false, "Ce téléphone existe déjà!"),
                    HttpStatus.BAD_REQUEST);
        }
        
        if(userRepository.existsByEmail(signUpRequest.getEmail())) {
            return new ResponseEntity<Object>(new ApiResponse(false, "Cet email existe déjà!"),
                    HttpStatus.BAD_REQUEST);
        }
        
    	if(signUpRequest.getPassword().length() < 5) {
    		return new ResponseEntity<Object>(new ApiResponse(false, "Mot de passe trop court!"),
    				HttpStatus.BAD_REQUEST);
    	} 
        
        if (!this.roleRepository.existsByName("ROLE_SUPER_ADMIN")) {
        	List<Role> roles = Arrays.asList(new Role("ROLE_SUPER_ADMIN"), new Role("ROLE_ADMIN"), new Role("ROLE_TRANSPORTEUR"));
        	this.roleRepository.saveAll(roles); 
        }

        // Creating user's account
        User user = new User();
        user.setAll(signUpRequest);  
        
        if (signUpRequest.getIdStructure() != null) {
        	Structure s = this.structureRep.findById(signUpRequest.getIdStructure()).get();
        	user.setStructure(s);
        }
        
        user.setPassword(passwordEncoder.encode(signUpRequest.getPassword()));

		Role userRole = roleRepository.findByName(signUpRequest.getRoleName()).get();
		user.setRoles(Collections.singleton(userRole));      

        User userCreated = this.userRepository.save(user);

    	SignUpRequest s = new SignUpRequest(userCreated);
    	
        return ResponseEntity.ok().body(new ApiResponse(true, "Utilisateur inscrit avec succès!", s));
    }

    @GetMapping("/user/check/{id}")
    // @PreAuthorize("hasAnyRole('SUPER_ADMIN','CLIENT')")
    public ResponseEntity<?> existsByIdAndActive(@PathVariable("id") Long id) {
    	Boolean userExists = userRepository.existsByIdAndActive(id, true);
    	
    	return new ResponseEntity<Object>(new ApiResponse(userExists, userExists ? "" : "Utilisateur introuvable ou suspendu"), 
    			(userExists) ? HttpStatus.OK : HttpStatus.NOT_FOUND);
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
    	User user = this.userRepository.findById(id).get();
    	return ResponseEntity.ok().body(new ApiResponse(true, "", user));
    }


}
